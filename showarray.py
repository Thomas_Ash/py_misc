import sys
import datetime
from cStringIO import StringIO
import numpy as np
import scipy.ndimage as nd
import PIL.Image
from IPython.display import clear_output, Image, display

#Utility functions for exploring trained neural networks in ipython
#take a numpy matrix and plot it to an image in a few different ways

def showrgbarray(a, scale=1.0, debug=0, fmt='png'):
    an = np.uint8(np.clip(np.transpose(a, (1, 2, 0)), 0, 255))
    f = StringIO()
    PIL.Image.fromarray(an).save(f, fmt)
    h = a.shape[0]*scale
    w = a.shape[1]*scale
    display(Image(data=f.getvalue(), width=w, height=h))

def showarray(a, scale=1.0, debug=0, fmt='png'):
    amax = float(np.amax(a))
    if debug:
        print a.shape, amax

    an = np.uint8(np.clip((a/amax)*255, 0, 255))
    f = StringIO()
    PIL.Image.fromarray(an).save(f, fmt)
    h = a.shape[0]*scale
    w = a.shape[1]*scale
    display(Image(data=f.getvalue(), width=w, height=h))

    
def showsignedarray(a, scale=1.0, debug=0, fmt='png'):
    amin = float(np.amin(a))
    amax = float(np.amax(a))
    if amin > 0: 
        amin = 1.0
    if amax < 0: 
        amax = -1.0
    if debug:
        print a.shape, amin, amax
    r = np.zeros_like(a, dtype=np.float)
    g = np.zeros_like(a, dtype=np.float)
    b = np.zeros_like(a, dtype=np.float)
    
    for i in range(a.shape[0]):
        for j in range(a.shape[1]):
            if a[i, j] > 0:
                g[i, j] = float(a[i, j]) / amax if amax > 0 else 0.0
            else:
                r[i, j] = float(a[i, j]) / amin if amin < 0 else 0.0
    
    rgb = np.asarray([r.T, g.T, b.T]).T
    rgba = np.uint8(np.clip(rgb*255.0, 0, 255))
    #rgb = g
    f = StringIO()
    PIL.Image.fromarray(rgba).save(f, fmt)
    h = a.shape[0]*scale
    w = a.shape[1]*scale
    display(Image(data=f.getvalue(), width=w, height=h))  
    
