import bpy  
import math

# Script written for Blender 2.78
# Generates 3D models for drinking glasses - for use in VR game

def doGeometry(shape, segments):
	verts = []
	faces = []

	#generate verts
	for i in range(len(shape)):
		if i is 0 or i is len(shape) - 1:
			#radius of first & last elements can be assumed to be zero - single vertex
			verts.append((0, 0, shape[i][0]))
		else:
			for j in range(segments):
				theta = math.pi * 2 * j / segments
				verts.append((math.sin(theta) * shape[i][1], math.cos(theta) * shape[i][1], shape[i][0]))
	
	#len(verts) is (len(shape) - 2) * segments + 2
	
	# 0				0 first
	# 1 2 3 4		1
	# 5 6 7 8		2 last
	# 9				3
	
	#generate faces
	for i in range(len(shape) - 1):
		if i is 0:	#first
			for j in range(segments):
				faces.append((0, j+1, (j+1) % segments + 1 ))
		elif i is len(shape) - 2:	#last
			for j in range(segments):
				faces.append(((i - 1) * segments + j + 1, i * segments + 1, (i - 1) * segments + (j+1) % segments + 1))
		else:
			for j in range(segments):
				faces.append(((i - 1) * segments + j + 1, i * segments + j + 1, (i - 1) * segments + (j+1) % segments + 1))
				faces.append((i * segments + j + 1, i * segments + (j+1) % segments + 1, (i - 1) * segments + (j+1) % segments + 1))

	
	return verts, faces
	

	#(height, radius)
pintGlass = [
	(0, 0),
	(0, 0.05),
	(0.2, 0.07),
	(0.2, 0.065),
	(0.005, 0.045),
	(0.005, 0)
]

pintGlassInside = [
	(0.005, 0),
	(0.005, 0.045),
	(0.2, 0.065),
    (0.2, 0)

]

shotGlass = [
	(0, 0),
	(0, 0.02),
	(0.05, 0.03),
	(0.05, 0.025),
	(0.01, 0.015),
	(0.01, 0)
]

shotGlassInside = [
	(0.01, 0),
	(0.01, 0.015),
	(0.05, 0.025),
    (0.05, 0)
]

wineGlass = [
	(0, 0),
	(0, 0.05),
	(0.005, 0.05),
	(0.01, 0.01),
	(0.08, 0.01),
	(0.1, 0.03),
	(0.14, 0.05),
	(0.18, 0.07),
	(0.2, 0.06),
	(0.2, 0.055),
	(0.18, 0.065),
	(0.14, 0.045),
	(0.1, 0.025),
	(0.09, 0)
]

wineGlassInside = [
	(0.09, 0),
	(0.1, 0.025),
	(0.14, 0.045),
	(0.18, 0.065),
	(0.2, 0.055),
	(0.2, 0)

]

cocktailGlass = [
	(0, 0),
	(0, 0.05),
	(0.005, 0.05),
	(0.01, 0.01),
	(0.08, 0.01),
	(0.18, 0.09),
	(0.18, 0.085),
	(0.09, 0)
]

cocktailGlassInside = [
	(0.09, 0),
	(0.18, 0.085),
	(0.18, 0)
]


#verts, faces = doGeometry(pintGlass, 10)
#verts, faces = doGeometry(pintGlassInside, 10)
#verts, faces = doGeometry(shotGlass, 8)
#verts, faces = doGeometry(shotGlassInside, 8)
#verts, faces = doGeometry(wineGlass, 10)
#verts, faces = doGeometry(wineGlassInside, 10)
#verts, faces = doGeometry(cocktailGlass, 10)
verts, faces = doGeometry(cocktailGlassInside, 10)

mesh_data = bpy.data.meshes.new("glass_mesh_data")
mesh_data.from_pydata(verts, [], faces)
mesh_data.update()

obj = bpy.data.objects.new("Glass", mesh_data)

scene = bpy.context.scene
scene.objects.link(obj)
obj.select = True
